![](images/user-login-alert.png)
# This script will alert you in case someone login into your servers and it will notified you via Slack Channel and email

## Table of Content
- [Plan](#plan)
  - [Table of Content](#table-of-content)
  - [Step Cero (0)](#step-cero-0)
  - [Execution](#execution)
### Plan
This project will **It will create a Gitlab Repository and it will integrate after with Slack
### Step Cero (0)
Make sure you have a [Gitlab Access Token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html), and [Terraform](https://terraform.io) configured:

### Configurable Variables that you must set:
* **ENV_SlackWebHookToken_URL** = [Slack WebHook](https://api.slack.com/incoming-webhooks)

### Execution
```
 ansible-playbook -u _your_privileged_sudo_user_ -i '_ip_server_,' accessAlert.yml
```

