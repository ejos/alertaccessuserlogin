#!/usr/bin/python

import smtplib
import ssl
import socket
import getpass

port = 587
smtp_server = "{{ smtp_server }}"
sender_email = "{{ email }}"
receiver_email = "{{ receiver_email }}"
password = "{{ password }}"
hostname = socket.gethostname()
uid = getpass.getuser()
message = """\
Subject: Hi there

There is an access from: %s in %s""" %( uid,hostname )

#context = ssl.SSLContext(ssl.PROTOCOL_TLS)
try:
  with smtplib.SMTP(smtp_server, port) as server:
    server.ehlo()
    server.starttls()
    server.ehlo()
    server.login(sender_email,password)
    server.sendmail(sender_email, receiver_email, message)
except smtplib.SMTPException as e:
    print (e)
