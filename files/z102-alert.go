package main

import (
    "bytes"
    "encoding/json"
    "errors"
    "log"
    "net/http"
    "time"
    "os"
    "os/user"
    "strings"
//    "fmt"
)


type SlackRequestBody struct {
    Text string `json:"text"`
}


type SlackMessageBody struct {
   Text string `json:"text"`
   Attachments []Attachment `json:"attachments"`
}

type Attachment struct {
	Title    string   `json:"title"`
	Text     string   `json:"text"`
	MrkdwnIn []string `json:"mrkdwn_in"`
	Fields   []Field  `json:"fields"`
}

type Field struct {
	Title string `json:"title"`
	Value string `json:"value"`
	Short bool   `json:"short"`
}


func buildMessage() SlackMessageBody {

    _h_ , _  := os.Hostname()
    _uid_current_, _ := user.Current()

    _uid_sudo_:= os.Getenv("SUDO_USER")

    message := SlackMessageBody{}

    if  _, ok:= os.LookupEnv("SSH_CLIENT"); ok {
        _ip_info_:= os.Getenv("SSH_CLIENT")
        _ip_client_ := strings.Split( _ip_info_  , " ")

        message = SlackMessageBody{
           Attachments: []Attachment{
             { Title: "SECURITY ALERT \t\n Incident Has Occured", MrkdwnIn: []string{ "text"},
               Fields: []Field{
		  { Title: "User", Value: _uid_current_.Username , Short: true},
		  { Title: "On Host", Value: _h_, Short: true},
	          { Title: "From Host", Value: _ip_client_[0], Short: true},
               },
             },
           },
        }
    }else {
        message = SlackMessageBody{
           Attachments: []Attachment{
             { Title: "SECURITY ALERT \t\n Incident Has Occur", MrkdwnIn: []string{ "text"},
               Fields: []Field{
	          { Title: "On Host" , Value:  _h_, Short: true},
	          { Title: "From User" , Value:  _uid_sudo_, Short: true},
		  { Title: "Now As User" , Value:  _uid_current_.Username , Short: true},
               },
             },
           },
        }
    }
    return message
}

func main() {

  message := buildMessage()

/*
    //WebhookTest notify on slack channel -> #devops-demo 
*/
    webhookUrl := "{{ _env_slack_webhook_token_url_ }}"

    err := SendSlackNotification(webhookUrl, message )

    if err != nil {
        log.Fatal(err)
    }

}

func SendSlackNotification(webhookUrl string,  msg SlackMessageBody ) error {

    slackBody, _ := json.Marshal( msg )
    req, err := http.NewRequest(http.MethodPost, webhookUrl, bytes.NewBuffer(slackBody))
    if err != nil {
        return err
    }

    req.Header.Add("Content-Type", "application/json")

    client := &http.Client{Timeout: 10 * time.Second}
    resp, err := client.Do(req)
    if err != nil {
        return err
    }

    buf := new(bytes.Buffer)
    buf.ReadFrom(resp.Body)
    if buf.String() != "ok" {
        return errors.New("Non-ok response returned from Slack")
    }
    return nil 
}
