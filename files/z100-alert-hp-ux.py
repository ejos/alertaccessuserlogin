#!/usr/local/bin/python

import smtplib
import ssl
import socket
import getpass
import os

from smtplib import SMTP_SSL as SMTP 

port = 587
smtp_server = "{{ smtp_server }}"
sender_email = "{{ email }}"
receiver_email = "{{ receiver_email }}"
password = "{{ password }}"
hostname = socket.gethostname()
uid = getpass.getuser()
_env_vars_ = os.environ
remote_ip = _env_vars_["SSH_CLIENT"].split(' ')[0]

message = """\
Subject: Hi there

There is an access from ip: %s in %s with the user: %s""" %( remote_ip,hostname,uid )

server = smtplib.SMTP(smtp_server,port)
try: 
# server.set_debuglevel(True)
  server.ehlo()
  server.starttls()
  server.ehlo() 
  server.login(sender_email,password)
  server.sendmail(sender_email, receiver_email, message)
except smtplib.SMTPException as e:
  print (e)
